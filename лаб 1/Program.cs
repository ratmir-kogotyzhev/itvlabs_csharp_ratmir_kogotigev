﻿/*•	Написать приложение, которое находит сумму первых трех цифр дробной части вещественного числа. 
 * Например, для числа 23,16809 она равна 15.*/

using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите дробное число");
            double b = Convert.ToDouble(Console.ReadLine());
            double a = Math.Truncate((b - Math.Truncate(b)) * 1000);
            double c = Math.Truncate(a / 100) + (Math.Truncate(a / 10) - (Math.Truncate(a / 100) * 10)) + (a - (Math.Truncate(a / 10) * 10));
            Console.WriteLine(c);
            Console.ReadKey();
        }
    }
}
